## Publish (static) Rmarkdown Website

[Rmarkdown](https://bookdown.org/yihui/rmarkdown/) is a great way to build
reproducible, self-documenting R code and optionally render the output of the
code in various formats.

This example uses the ioslides rmarkdown type to render an Rmarkdown document into html.

Everytime you push to this repo a [build job](.gitlab-ci.yml) starts that will try to
build a static, non-interactive website based on the example slides.Rmd.

Because we export a specific *artifact* called **public** and named the *job*
**pages**, gitlab will automatically publish that document to the web, no
further configuration necessary.

Check it out here: https://citemplates.mpib.berlin/static_rmarkdown


![screenshot](screenshot.png)



### Notes

This approach only works for static websites (non-interactive, no shiny
components) that do not require an Rserver instance to calculate things.
